var express = require('express');
var app = express();
var Q = require('q');
var fs = require("fs");
var deploy = require('./deploy');
var async = require('async');
var bodyParser = require('body-parser');
var dateFormat = require('dateformat');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.get('/listUsers', function (req, res) {
	var test = getAllUsers();
	console.log(test);

});



app.get('/checkForLandingPageWithoutMicrositeForChildOfHyperlink',function(req,res){
	deploy.checkForLandingPageWithoutMicrositeForChildOfHyperlink(req.query.packageId)
	.then(function(result){
		//console.log(result);
		res.send();
	});

})


app.get('/updateUnsupportedAssets',function(req,res){
	console.log(req.query.packageId);
	deploy.updateUnsupportedAssets(req.query.packageId)
	.then(function(){
		res.send();
	});
})

app.get('/preDeployPackageItems',function(req,res){
	console.log(req.query.packageId);
	deploy.preDeployPackageItems(req.query.packageId,req.query.queueId)
	.then(function(result){
		//console.log(result);
		res.send();
	});

})

app.get('/runAllValidations',function(req,res){
	console.log(req.query.packageId);
	deploy.runAllValidations(req.query.packageId)
	.then(function(result){
		//console.log(result);
		res.send();
	});

})

app.get('/handleDeploy',function(req,res){
	console.log(req.query.packageId);
	deploy.handleDeploy(req.query.packageId)
	.then(function(result){
		res.send();
	});

})

app.post('/QuickLogs',function(req,res){
	console.log(req.body);
	res.send();

})

app.post('/testDate',function(req,res){
	var now = new Date();
 
// Basic usage
   
	console.log(dateFormat(new Date(), "yyyy-mm-dd hh:MM:ss"));
	res.send();

})



var server = app.listen(8080, function () {

  var host = server.address().address
  var port = server.address().port

  console.log("Example app listening at http://%s:%s", host, port)

})

var getAllUsers = function(){

	return "No users found";
}
