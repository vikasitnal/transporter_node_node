(function(eloquaCall) {
	var request = require('request-promise');
	var error = require('request-promise/errors');
	eloquaCall.get = function(userInformation,resultantFunction) {
		if(userInformation.oauth){
			var authenticationHeader = "Bearer " + userInformation.token;
			var options = {  
			    url : userInformation.host,  
		        headers : { "Authorization" : authenticationHeader }  
		    };
			// console.log('request(options, resultantFunction)');
			// console.log(request(options, resultantFunction));
			return request(options, resultantFunction)
					.catch(function(error) {
						console.error(new Date()+': Error in Eloqua ->',error);
					});
		} else{
			var authenticationHeader = "Basic " + new Buffer(userInformation.username + ":" + userInformation.password).toString("base64");
			var options = {  
			    url : userInformation.host,  
		        headers : { "Authorization" : authenticationHeader }  
		    };
			return request(options, resultantFunction)
					.catch(function(error) {
						console.error(new Date()+': Error in Eloqua ->',error);
					});
		}
	}

	eloquaCall.put = function(userInformation,resultantFunction) {
		if(userInformation.oauth){
			var authenticationHeader = "Bearer " + userInformation.token;
			
			var options = {  
				url 		: userInformation.host,  
				headers 	: { "Authorization" : authenticationHeader } ,
				method		: 'PUT',
				body		: userInformation.body
			}
			// console.log(options);
			return request(options, resultantFunction)
					.catch(function(error) {
						console.error(new Date()+': Error in Eloqua ->',error);
					});
		} else{
			var authenticationHeader = "Basic " + new Buffer(userInformation.username + ":" + userInformation.password).toString("base64");
			var options = {
				url 		: userInformation.host,  
				headers 	: { "Authorization" : authenticationHeader },
				method		: 'PUT',
				body		: userInformation.body
			}
			return request(options, resultantFunction)
					.catch(function(error) {
						console.error(new Date()+': Error in Eloqua ->',error);
					});
		}
	}

	eloquaCall.post = function(userInformation,resultantFunction) {

		if(userInformation.oauth){
			
			var authenticationHeader = "Bearer " + userInformation.token;
			var options = {  
				url 		: userInformation.host,  
				headers 	: { "Authorization" : authenticationHeader } ,
				method		: 'POST',
				body		: userInformation.body,
				json		: true,
			}


			return request(options, resultantFunction)
					.catch(function(error) {
						console.error(new Date()+': Error in Eloqua ->',error);
					});
			
		} 
		else if(userInformation.data){

			var authenticationHeader = "Bearer " + userInformation.token;
			var options = {  
				url 		: userInformation.host,  
				headers 	: { "Authorization" : authenticationHeader } ,
				method		: 'POST',
				body		: userInformation.body,
				json		: false,
			}
			return request(options, resultantFunction)
					.catch(function(error) {
						console.error(new Date()+': Error in Eloqua ->',error);
					});
			
		}
		else 
		{
			var authenticationHeader = "Basic " + new Buffer(userInformation.username + ":" + userInformation.password).toString("base64");
			var options = {
				url 		: userInformation.host,  
				headers 	: { "Authorization" : authenticationHeader } ,
				method		: 'POST',
				body		: userInformation.body,
				json		: true,
			}
			return request(options, resultantFunction)
					.catch(function(error) {
						console.error(new Date()+': Error in Eloqua ->',error);
					});
		}
	}
	
	
	
	eloquaCall.delete = function(userInformation,resultantFunction) {
		var authenticationHeader = "Basic " + new Buffer(userInformation.username + ":" + userInformation.password).toString("base64");
		var options = {  
	        url : userInformation.host,  
	        headers : { "Authorization" : authenticationHeader } ,
	        method:'DELETE',
			json: true,
	    }
		return request(options,resultantFunction)
				.catch(function(error) {
					console.error(new Date()+': Error in Eloqua ->',error);
				});
	}
})(module.exports);