(function (deploy) {
	var Q = require('q');
	var fs = require('fs');
	var db = require('./dbConnection');
	var http = require('http');
	var request = require('request-promise');
	var async = require('async');
	var dateFormat = require('dateformat');
	var eloquaCallBasic = require('./eloquaCallBasic');
	var jp = require('jsonpath');
	var _ = require('underscore');
	// var access = fs.createWriteStream('/debug.log', { flags: 'a' })
	// process.stdout.pipe(access);
	//----------Global variables-----------------//
	var globalStack = [];
	var childAssetCalled = [];
	var recursionCounter = 0;
	var recursionTrack = [];
	var globalarray = [];
	var totalChild = 0;
	
	var fs = require('fs');
    var util = require('util');
    var log_file = fs.createWriteStream( './debug.log', {flags : 'a'});
    var log_stdout = process.stdout;

    console.log = function(d) { //
    log_file.write(d + '\n');
    };

	//----------Global Variables end-------------//

	//----------Database calls-------------------//


	deploy.updateDeploymentPackage = function (packageId) {
		var deferred = Q.defer();
		var query = "update deployment_package SET Status = ? WHERE Deployment_Package_Id = ?";
		var params = ['Unsupported',packageId]
		db.query(query,params).then(function (Asset_Type) {
			deferred.resolve();
		}, function (err) {
			deferred.reject(err);
		});
		return deferred.promise;
	}

	deploy.updateDeploymentPackage_new = function (packageId) {
		var deferred = Q.defer();
		var query = "update deployment_package SET Status = ? WHERE Deployment_Package_Id = ?";
		db.query(query,['Unsupported',packageId]).then(function (Asset_Type) {
			deferred.resolve();
		}, function (err) {
			deferred.reject(err);
		});
		return deferred.promise;
	}

	deploy.getPackage = function (packageId) {
		var deferred = Q.defer();
		var query = "select * from deployment_package WHERE Deployment_Package_Id = ?";
		var params = [packageId];
		db.query(query,params).then(function (pack) {
			deferred.resolve(pack);
		}, function (err) {
			deferred.reject(err);
		});
		return deferred.promise;
	}

	deploy.getInstance = function (Site_Name) {
		var deferred = Q.defer();
		var query = "select * from instance_ WHERE Site_Name = ?";
		var params = [Site_Name];
		db.query(query,params).then(function (instance) {
			deferred.resolve(instance);
		}, function (err) {
			deferred.reject(err);
		});
		return deferred.promise;
	}

	deploy.updateDeploymentPackageItem = function (packageItemId, pathValue, message) {
		var deferred = Q.defer();
		var query = "select Unsupported_Asset_Message from deployment_package_item WHERE Deployment_Package_Item_Id = ?";
		var params = [packageItemId];
		db.query(query,params).then(function (packageItem) {
			if ((packageItem[0].Unsupported_Asset_Message) && (packageItem[0].Unsupported_Asset_Message.length > 0)) {
				if (packageItem[0].Unsupported_Asset_Message != message) {
					if (packageItem[0].Unsupported_Asset_Message.indexOf(',') !== -1) {
						var messages = packageItem[0].Unsupported_Asset_Message.split(',');
						if (messages.indexOf(message) == -1) {
							message = packageItem[0].Unsupported_Asset_Message + "," + message;
						}
						else {
							message = packageItem[0].Unsupported_Asset_Message;
						}
					} else {
						message = packageItem[0].Unsupported_Asset_Message + "," + message;
					}
				}
			}
			var query = "update  deployment_package_item SET Unsupported_Asset_Message = ?, Status = ?, verified=1 WHERE Deployment_Package_Item_Id = ?";
			var params2 = [message.replace(new RegExp("'", "g"), "''"),'Unsupported',packageItemId];
			db.query(query,params2).then(function (updated) {
				deferred.resolve();
			}, function (err) {
				deferred.reject(err);
			})
		}, function (err) {
			deferred.reject(err);
		});
		return deferred.promise;
	}

	//This gets all the package items using package id
	deploy.getPackageItems = function (packageId) {
		var deferred = Q.defer();
		var query = "select * from deployment_package_item where Deployment_Package_Id = " + packageId;
		db.query(query)
			.then(function (result) {
				deferred.resolve(result);
			});
		return deferred.promise;
	}

	deploy.getVerifiedPackageItems = function (packageId) {
		var deferred = Q.defer();
		var query = "select * from deployment_package_item where Deployment_Package_Id = " + packageId + " AND (verified=1 or missing_target = 1)";
		db.query(query)
			.then(function (result) {
				deferred.resolve(result);
			});
		return deferred.promise;
	}

	deploy.getPackageItem = function (packageId, assetType, assetId) {
		var deferred = Q.defer();
		var query = "select Deployment_Package_Id,Deployment_Package_Item_Id,Asset_Type,Asset_Name,Asset_Id,Deploy_Stage from deployment_package_item where Deployment_Package_Id = " + packageId + " and Asset_Type = ? and Asset_Id = '" + assetId + "'";
		var params = [assetType];
		db.query(query,params)
			.then(function (result) {
				deferred.resolve(result);
			})
			.catch(function (err) {
				deferred.resolve();
			});
		return deferred.promise;
	}

	//This updates status as Valid
	deploy.updateValidatingStatus = function (packageItemId) {
		var query = "Update deployment_package_item SET Status= 'Validating' WHERE Deployment_Package_Item_Id=" + packageItemId;
		db.query(query);
	}

	deploy.updateValidStatus = function (packageId) {
		var deferred = Q.defer();
		var query = "Update deployment_package_item SET Status= 'Valid' WHERE Status = 'Validating' and Deployment_Package_Id=" + packageId;
		db.query(query)
			.then(function (res) {
				deferred.resolve();
			});
		return deferred.promise;
	}

	deploy.updateDPValidationList = function (packageItem, childval) {

		var selectQuery = "Select Deployment_Package_Item_Id from deployment_package_validation_list";
		selectQuery += " where Deployment_Package_Item_Id=" + packageItem.Deployment_Package_Item_Id;
		selectQuery += " and Asset_Type='" + childval.Asset_Type + "'";
		selectQuery += " and Asset_Id=" + childval.Asset_Id;


		db.query(selectQuery)
			.then(function (selectResult) {
				if (selectResult.length > 0) {

				}
				else {
					var insertQuery = "INSERT into deployment_package_validation_list (Deployment_Package_Item_Id,Deployment_Package_Id,Asset_Type,Asset_Id,Asset_Name,JSON_Node_Value,Status) values";
					insertQuery += " ( " + packageItem.Deployment_Package_Item_Id + ", " + packageItem.Deployment_Package_Id + ", '" + childval.Asset_Type + "', " + childval.Asset_Id + ", ?, '" + childval.JSON_Node_Value + "', 'New')";
					var params = [ childval.Display_Name.replace(new RegExp("'", "g"), "''")];
					db.query(insertQuery,params);
				}

			})
			.catch(function (err) {

			})
	}

	deploy.getDeployPackageValidationList = function (packageItem) {
		var deferred = Q.defer();
		var query = "select * from deployment_package_validation_list where Deployment_Package_Item_Id = " + packageItem;
		db.query(query)
			.then(function (result) {
				deferred.resolve(result);
			})
			.catch(function (err) {
				deferred.resolve();
			});
		return deferred.promise;
	}

	deploy.addOrUpdatePackage = function (packageId, assetType, assetId, assetName) {
		var deferred = Q.defer();
		var selectQuery = "select Asset_Type from deployment_package_item where Asset_Type = '" + assetType + "' and Asset_Id = " + assetId + " and Deployment_Package_Id = " + packageId.Deployment_Package_Id + "";
		db.query(selectQuery)
			.then(function (selectResult) {
				if (selectResult.length > 0) {

				}
				else {
					var insertQuery = "Insert into deployment_package_item (Deployment_Package_Id,Asset_Type,Asset_Id,Asset_Name) values";
					insertQuery += " (" + packageId.Deployment_Package_Id + ",'" + assetType + "'," + assetId + ", ?)";
					var params = [assetName.replace(new RegExp("'", "g"), "''")];
					db.query(insertQuery,params)
						.then(function (result) {
							deferred.resolve(true);
						});
				}
			});

		return deferred.promise;
	}



	deploy.updateOrder = function (Deployment_Package_Item_Id, index) {
		var deferred = Q.defer();
		var updateQuery = "UPDATE deployment_package_item SET Deploy_Ordinal = " + index + " WHERE Deployment_Package_Item_Id = " + Deployment_Package_Item_Id;
		//console.log(updateQuery);
		db.query(updateQuery)
			.then(function (result) {
				deferred.resolve(true);
			}, function (err) {
				//console.log(err);
			});

		return deferred.promise;
	}

	//gets the details of the instance along with tokens
	deploy.getInstanceDetails = function (packageId, type) {
		var deferred = Q.defer();
		var selectQuery = "";
		if (type == 'source') {
			selectQuery = "SELECT dp.auto_include_missing_assets,t.Base_Url as baseUrl, t.Token as accessToken,t.Refresh_Token as refreshToken,t.Token_Expiry_Time as tokenExpiryTime";
			selectQuery += " FROM deployment_package dp";
			selectQuery += " LEFT OUTER JOIN instance_ t ON dp.Source_Site_Name = t.Site_Name ";
			selectQuery += " WHERE dp.Deployment_Package_Id = " + packageId;
		}
		else {
			selectQuery = "SELECT t.Base_Url as baseUrl, t.Token as accessToken,t.Refresh_Token as refreshToken,t.Token_Expiry_Time as tokenExpiryTime";
			selectQuery += " FROM deployment_package dp";
			selectQuery += " LEFT OUTER JOIN instance_ t ON dp.Target_Site_Name = t.Site_Name ";
			selectQuery += " WHERE dp.Deployment_Package_Id = " + packageId;
		}

		db.query(selectQuery)
			.then(function (result) {
				deferred.resolve(result);
			})
			.catch(function (err) {
				deferred.reject(err);
			});
		return deferred.promise;
	}

	//gets the asset endpoint configured for a particular asset type
	deploy.getAssetEndpoint = function (assetType) {
		var deferred = Q.defer();
		var selectQuery = "SELECT r.Endpoint_URL as endpointUrl FROM rsys_asset_endpoint r WHERE r.Asset_Type ='" + assetType + "'";
		selectQuery += " AND r.Endpoint_Type = 'Find Asset'";
		db.query(selectQuery)
			.then(function (result) {
				deferred.resolve(result);
			})
			.catch(function (err) {
				deferred.reject(err);
			})
		return deferred.promise;
	}

	deploy.getAssetEndpointSingle = function (assetType) {
		var deferred = Q.defer();
		var selectQuery = "SELECT r.Endpoint_URL as endpointUrl FROM rsys_asset_endpoint r WHERE r.Asset_Type ='" + assetType + "'";
		selectQuery += " AND r.Endpoint_Type = 'Read Single'";
		db.query(selectQuery)
			.then(function (result) {
				deferred.resolve(result);
			})
			.catch(function (err) {
				deferred.reject(err);
			})
		return deferred.promise;
	}

	deploy.updateTargetAssetIdDPValidationList = function (targetAssetId, packageId, Asset_Type, Asset_Id) {
		var deferred = Q.defer();
		var updateQuery = "UPDATE deployment_package_validation_list";
		updateQuery += " SET Target_Asset_Id=" + targetAssetId;
		updateQuery += " WHERE Deployment_Package_Id=" + packageId;
		updateQuery += " AND Asset_Type='" + Asset_Type + "'";
		updateQuery += " AND Asset_Id=" + Asset_Id;
		db.query(updateQuery)
			.then(function (result) {
				deferred.resolve(true);
			})
			.catch(function (err) {
				//console.log(err);
			});

		return deferred.promise;
	}

	deploy.updateAssetJsonAndStatus = function (packageItemId, asset_json) {
		var deferred = Q.defer();
		var updateQuery = "UPDATE deployment_package_item";
		updateQuery += " SET JSON_Asset='" + JSON.stringify(asset_json) + "'";
		updateQuery += ", Status='Valid'";
		updateQuery += " WHERE Deployment_Package_Item_Id=" + packageItemId;
		db.query(updateQuery)
			.then(function (result) {
				deferred.resolve(true);
			})
			.catch(function (err) {
				//console.log(err);
			});

		return deferred.promise;
	}

	deploy.updateMissingTargetFlag = function (flag, packageItemId) {
		var deferred = Q.defer();
		var updateQuery = "UPDATE deployment_package_item";
		updateQuery += " SET missing_target=" + flag;
		updateQuery += " WHERE Deployment_Package_Item_Id=" + packageItemId;
		db.query(updateQuery)
			.then(function (result) {
				deferred.resolve(true);
			}, function (err) {
				//console.log(err);
			});

		return deferred.promise;
	}

	deploy.updateVerifiedFlag = function (flag, packageItemId) {
		var deferred = Q.defer();
		var updateQuery = "UPDATE deployment_package_item";
		updateQuery += " SET verified=" + flag;
		updateQuery += " WHERE Deployment_Package_Item_Id=" + packageItemId;
		updateQuery += " AND missing_target = 1";
		updateQuery += " AND verified = 0";
		db.query(updateQuery)
			.then(function (result) {
				deferred.resolve(true);
			}, function (err) {
				//console.log(err);
			});

		return deferred.promise;
	}

	deploy.getAssetJSONByIdType = function (packageId, Asset_Type, Asset_Id) {
		var deferred = Q.defer();
		var query = "select JSON_Asset from deployment_package_item where Deployment_Package_Id = " + packageId;
		query += " AND Asset_Type='" + Asset_Type + "' and Asset_Id=" + Asset_Id;
		db.query(query)
			.then(function (result) {
				deferred.resolve(result);
			});
		return deferred.promise;
	}

	deploy.UpdateDuplicatesArray = function (duplicates, packageItemId, status) {
		var deferred = Q.defer();
		var targetAssetDetails = {};
		targetAssetDetails.duplicates_from_target = duplicates;
		targetAssetDetails.parent_asset_type = '';
		targetAssetDetails.parent_asset_name = '';
		var updateQuery = ""
		if (status === "Duplicate") {
			updateQuery = "UPDATE deployment_package_item";
			updateQuery += " SET missing_target = 0,";
			updateQuery += " Status='" + status + "',";
			updateQuery += " verified = 1,";
			updateQuery += " Target_Duplicate_Assets=?";
			updateQuery += " WHERE Deployment_Package_item_Id = " + packageItemId;
		}
		else {
			updateQuery = "UPDATE deployment_package_item";
			updateQuery += " SET missing_target = 0,";
			updateQuery += " verified = 1,";
			updateQuery += " Target_Duplicate_Assets=?";
			updateQuery += " WHERE Deployment_Package_item_Id = " + packageItemId;
		}

		var params = [JSON.stringify(targetAssetDetails)];

		db.query(updateQuery,params)
			.then(function (result) {
				deferred.resolve(result)
			})
			.catch(function (err) {
				deferred.reject(err);
			});

		return deferred.promise;
	}

	deploy.updateDuplicatesOfOneLevelChild = function (duplicates, package, validationList) {
		var deferred = Q.defer();
		var targetAssetDetails = {};
		targetAssetDetails.duplicates_from_target = duplicates;
		targetAssetDetails.parent_asset_type = package.Asset_Type;
		targetAssetDetails.parent_asset_name = package.Asset_Name;

		var updateQuery = ""

		updateQuery = "UPDATE deployment_package_item";
		updateQuery += " SET missing_target = 0,";
		updateQuery += " verified = 1,";
		updateQuery += " Target_Duplicate_Assets=?";
		updateQuery += " WHERE Deployment_Package_Id = " + package.Deployment_Package_Id;
		updateQuery += " AND Asset_Type = '" + validationList.Asset_Type + "'";
		updateQuery += " AND Asset_Id = '" + validationList.Asset_Id + "'";
		var params = [JSON.stringify(targetAssetDetails)];
		db.query(updateQuery,params)
			.then(function (result) {
				updateQuery = "UPDATE deployment_package_item";
				updateQuery += " SET Status = 'Duplicate'";


				updateQuery += " WHERE Deployment_Package_Id = " + package.Deployment_Package_Id;
				updateQuery += " AND Target_Asset_Id = 0";
				updateQuery += " AND Asset_Type = '" + validationList.Asset_Type + "'";
				updateQuery += " AND Asset_Id = '" + validationList.Asset_Id + "'";

				db.query(updateQuery)
					.then(function (result2) {
						var selectQuery = " SELECT Target_Asset_Id FROM deployment_package_item WHERE deployment_package_id = " + package.Deployment_Package_Id;
						selectQuery += " AND Asset_Type= '" + validationList.Asset_Type + "'";
						selectQuery += " AND Asset_Id =" + validationList.Asset_Id;
						db.query(selectQuery)
							.then(function (result3) {
								if ((result3.length > 0) && result3[0].Target_Asset_Id > 0) {
									updateQuery = "UPDATE deployment_package_validation_list";
									updateQuery += " SET Target_Asset_Id = " + result3[0].Target_Asset_Id;
									updateQuery += " WHERE Deployment_Package_Id = " + package.Deployment_Package_Id;
									updateQuery += " AND Asset_Type = '" + validationList.Asset_Type + "'";
									updateQuery += " AND Asset_Id = '" + validationList.Asset_Id + "'";
									db.query(updateQuery)
										.then(function (result4) {
											deferred.resolve();

										})
								}
								else {
									deferred.resolve();
								}

							})
					});
			});
		return deferred.promise;
	}

	deploy.checkForAssetsIncludedInPackage = function (packageId, assetType, assetId) {
		var deferred = Q.defer();
		var query = "select Asset_Type from deployment_package_item WHERE Asset_Type = '" + assetType + "' and Deployment_Package_Id = " + packageId + " and Asset_Id = " + assetId;
		db.query(query)
		.then(function (Asset_Type) {
			if (Asset_Type.length > 0) {
				deferred.resolve(1);
			} else {
				deferred.resolve(0);
			}
		}, function (err) {
			deferred.reject(err);
		});
		return deferred.promise;
	}

	deploy.updateChildElementStages = function (packageItemId, parentStage) {
		parentStage = parentStage + 1;
		var deferred = Q.defer();
		var query = "select Asset_Type,Asset_Id,Deployment_Package_Id from deployment_package_validation_list where Deployment_Package_Item_Id = " + packageItemId;
		db.query(query)
			.then(function (result) {
				async.forEachLimit(result, 1, function (item, callback) {
					var updateQuery = "update deployment_package_item set Deploy_Stage = " + parentStage + " where Deploy_stage <" + parentStage + " ";
					updateQuery += " and Asset_Type='" + item.Asset_Type + "'";
					updateQuery += " and Asset_Id=" + item.Asset_Id;
					updateQuery += " and Deployment_Package_Id=" + item.Deployment_Package_Id;
					db.query(updateQuery)
						.then(function () {
							callback();
						});
				}, function (err) { });
			});
	}
	//----------Datebase Calls enb---------------//

	deploy.handleDeploy = function (packageId) {

		var deferred = Q.defer();
		var counter = 0;
		var self = this;
		var packageItems
		self.getPackageItems(packageId)
			.then(function (result) {
				packageItems = result;
				async.forEach(packageItems, function (item, maincallback) {
					self.buildGlobalStack(item, packageId, maincallback);
				}, function () {


					deferred.resolve();

					//console.log("ordering completed");

					async.forEachOf(globalStack, function (id, index, callback) {
						self.updateOrder(id, index).
							then(function (response) {
								callback();
							});
					}, function () {
						//console.log("deploy ordinal updated");
					});

				})
				// //Loop for each elements added in the package to find its child elements
				// packageItems.forEach(function(item,index){
				//
				// 	/*self.getChildAssets(item.Asset_Type,item.Asset_Id,packageId,item.Deployment_Package_Item_Id)
				// 	.then(function(childAssets){
				// 		counter++;
				// 		if(counter == packageItems.length)
				// 		{
				// 			////console.log(Date.now());
				// 			deferred.resolve(childAssets);
				// 		}
				//
				// 	});*/
				// 	////console.log(' before call main '+ ++recursionCounter);
				//
				//
				// 	//deferred.resolve();
				// 	////console.log(' after call main '+ --recursionCounter);
				// 	//check for missing assets
				// });

			})
			.catch(function (err) {
				////console.log(err);
			});

		return deferred.promise;

	}

	deploy.function_new = function () {
		////console.log('This should be called after buildGlobalStack finishes');

	}

	deploy.buildGlobalStack = function (packageItem, packageId, callback) {
		////console.log('entry '+packageItem.Asset_Id);
		var self = this;
		////console.log(recursionTrack);
		//This will add the item to the global array
		self.putOnGlobalStack(packageItem.Deployment_Package_Item_Id);

		//Sets the status as valid - since it has added to the package
		self.updateValidatingStatus(packageItem.Deployment_Package_Item_Id);

		//Make a call to get all the child assets
		self.getChildAssets(packageItem.Asset_Type, packageItem.Asset_Id, packageId, packageItem.Deployment_Package_Item_Id)
			.then(function (childAssets) {

				var childAssetsCount = 0
				//This is to track on all the elemets for which the child assets function is called - this is to avoid recursive child functions
				childAssetCalled.push(packageItem.Deployment_Package_Item_Id);
				if (childAssets.length > 0) {
					recursionTrack.push(packageItem.Deployment_Package_Item_Id);
					////console.log('child of '+ packageItem.Asset_Name+' = '+childAssets.length);

					//Updating the child elemets into Validation list - To keep parent child relations
					childAssets.forEach(function (item, index) {
						self.updateDPValidationList(packageItem, item);
					});

					async.forEachLimit(childAssets, 3, function (item, childcallback) {
						//if(recursionTrack.indexOf(item.Asset_Id) < 0){
						var assetType = item.Asset_Type;
						var assetId = item.Asset_Id;
						childAssetsCount++;
						self.CheckAssetInTarget(packageId, assetType, item.Display_Name)
						.then(function(assetsExist){
							var childassetsNeeded = 0;
							if(packageItem.Asset_Type == 'Form' || packageItem.Asset_Type == 'Campaign' || packageItem.Asset_Type == 'Program' ){
								if(assetType == 'Program' || assetType == 'Campaign'){
									childassetsNeeded = 1;
								}
							}
							if(assetType == 'Custom Object'){
								childassetsNeeded = 1;
							}
							if(assetsExist && !childassetsNeeded ){
								childcallback();
							}
							else{
								self.getPackageItem(packageItem.Deployment_Package_Id, assetType, assetId)
								.then(function (childPackageItem) {
								if (childPackageItem.length > 0) {
									if (childAssetCalled.indexOf(packageItem.Deployment_Package_Item_Id) > -1) {
										// self.PushAssetChildAssetsOnGlobalStack(packageItem.Deployment_Package_Item_Id)
										// 	.then(function () {
										// 		
										// 	});
										childcallback();
									}
									else {
										self.buildGlobalStack(childPackageItem[0],packageId,function(){
											childcallback();
										});
									}
								}
								else {
									// create a package here for those items which are not added
									self.addOrUpdatePackage(packageItem, assetType, assetId, item.Display_Name)
										.then(function (result) {
											self.getPackageItem(packageId, assetType, assetId)
												.then(function (packageItem2) {
													self.buildGlobalStack(packageItem2[0], packageId, childcallback);
												});

										});
								}
							});
							}
						});
					}, function () {
						callback();
					})
				} else {
					callback();
				}
			})
			.catch(function (err) {
				////console.log(err);
			})
		}

	deploy.PushAssetChildAssetsOnGlobalStack = function (packageItem, stage) {
		var deferred = Q.defer();
		var self = this;
		self.updateStageNew(packageItem, stage)
			.then(function (result) {
				self.getDeployPackageValidationList(packageItem)
					.then(function (validationList) {
						if (validationList.length > 0) {
							validationList.forEach(function (item, index) {
								self.getPackageItem(item.Deployment_Package_Id, item.Asset_Type, item.Asset_Id)
									.then(function (packageItem) {
										if (packageItem.length > 0) {
											self.updateStageNew(packageItem[0].Deployment_Package_Item_Id, stage + 1).then(function () {
												deferred.resolve();
											});
										} else {
											deferred.resolve();
										}
									});
							});
						} else {
							deferred.resolve();
						}

					});
			});

		return deferred.promise;
	}

	deploy.putOnGlobalStack = function (deployment_package_item) {
		var deferred = Q.defer();
		const index = globalStack.indexOf(deployment_package_item);
		var globalindex = globalarray.indexOf(deployment_package_item);
		if (index !== -1) {
			globalStack.splice(index, 1);
		}
		globalStack.push(deployment_package_item);
		if (globalindex < 0) {
			globalarray.push(deployment_package_item);
		}
		deferred.resolve(true);
		return deferred.promise;
	}

	deploy.runAllValidations = function (packageId) {

		var deferred = Q.defer();
		var methods = [];
		var count = 0;
		var self = this;

		deploy.handleDeploy(packageId)
			.then(function () {
				deploy.updateMissingElements(packageId)
					.then(function () {
						methods.push(deploy.updateUnsupportedAssets);
						methods.push(deploy.checkDuplicatehtmlName);
						methods.push(deploy.EmailWithDifferentEG);
						methods.push(deploy.checkForCampaignInputs);
						methods.push(deploy.checkForLandingPageWithoutMicrositeForChildOfHyperlink);
						methods.push(deploy.assignStages);
						async.forEach(methods, function (item, callback) {
							item(packageId, self)
								.then(function () {
									console.log('-------------');
									callback();
								})
						}, function (err) {
							if (err) {
								deferred.reject(err);
							} else {
								self.updateValidStatus(packageId).then(function () {
									setTimeout(
										self.decideStatusValidate(packageId).then(function (status) {
											var query = "update deployment_package SET Status = '" + status + "' WHERE Deployment_Package_Id = " + packageId;
											db.query(query).then(function (response) {
												console.log('Validate Completed before actual completion');
												console.log(dateFormat(new Date(), "yyyy-mm-dd hh:MM:ss"));
												// deploy.handleStage(packageId)
												// .then(function(){
												// 	deferred.resolve();
												// })
												// .catch(function(){
												// 	deferred.resolve();
												// });
												deferred.resolve();
											}, function (err) {
												deferred.reject(err);
											});
										}, function (err) {
											var query = "update deployment_package SET Status = '" + err.message + "' WHERE Deployment_Package_Id = " + packageId;
											db.query(query).then(function (response) {
												deferred.resolve();
											}, function (err) {
												deferred.reject(err);
											});
										}), 5000);
								});

							}
						});

					});

			});
		return deferred.promise;
	}

	deploy.decideStatusValidate_old = function (packageId) {
		var deferred = Q.defer();
		var self = this;
		var status;
		self.getPackageItems(packageId).then(function (result) {
			async.forEach(result, function (item, callback) {
				if (item.missing_target == 1 && item.verified == 0) {
					status = "Missing Assets";
				} else {
					switch (item.Status) {
						case 'Invalid':
							status = "Invalid";
							break;
						case 'Unsupported':
							status = "Unsupported";
							break;
						case 'Valid':
							status = "Valid";
							break;
						case 'Duplicate':
							status = "Duplicates Found";
							break;
						default:
							status = "Valid";
					}
				}
				callback();

			}, function (err) {
				if (errored > 0) {
					deferred.resolve('Invalid');
				}
				else if (unsupported > 0) {
					deferred.resolve('Unsupported');
				}
				else if (missing_assets > 0) {
					deferred.resolve('Missing Assets');
				}
				else if (duplicates > 0) {
					deferred.resolve('Duplicates Found');
				}
				else {
					deferred.resolve('Validate Completed');
				}
			});
		}, function (err) {
			deferred.reject(err);
		})
	}

	deploy.decideStatusValidate = function (packageId) {
		var deferred = Q.defer();
		var self = this;
		var status;
		var errored = 0;
		var completed = 0;
		var unsupported = 0;
		var missing_assets = 0;
		var duplicates = 0;
		self.getPackageItems(packageId).then(function (result) {
			async.forEach(result, function (item, callback) {
				if (item.missing_target == 1 && item.verified == 0) {
					status = "Missing Assets";
					missing_assets++;
				}
				else {
					switch (item.Status) {
						case 'Invalid':
							status = "Invalid";
							errored++;
							break;
						case 'Unsupported':
							status = "Unsupported";
							unsupported++;
							break;
						case 'Valid':
							status = "Valid";
							completed++;
							break;
						case 'Duplicate':
							status = "Duplicates Found";
							duplicates++
							break;
						default:
							status = "Valid";
					}
				}
				callback();

			}, function (err) {
				if (errored > 0) {
					deferred.resolve('Invalid');
				}
				else if (unsupported > 0) {
					deferred.resolve('Unsupported');
				}
				else if (missing_assets > 0) {
					deferred.resolve('Missing Assets');
				}
				else if (duplicates > 0) {
					deferred.resolve('Duplicates Found');
				}
				else {
					deferred.resolve('Validate Completed');
				}
			});
		}, function (err) {
			deferred.reject(err);
		})
		return deferred.promise;
	}

	deploy.decideStatus = function (packageId) {
		var deferred = Q.defer();
		var self = this;
		var status;
		var errored = 0;
		var completed = 0;
		var action_required = 0;
		self.getPackageItems(packageId).then(function (result) {
			async.forEach(result, function (item, callback) {
				switch (item.Status) {
					case 'Errored':
						status = "Errored";
						errored++;
						break;
					case 'Completed':
						status = "Completed";
						completed++;
						break;
					case 'Action Required':
						status = "Action Required";
						action_required++;
						break;
				}
				callback();

			}, function (err) {
				if (errored > 0) {
					deferred.resolve('Errored');
				}
				else if (action_required > 0) {
					deferred.resolve('Action Required');
				}
				else {
					deferred.resolve('Deploy Completed');
				}
			});
		}, function (err) {
			deferred.reject(err);
		})
		return deferred.promise;
	}


	deploy.getChildAssets = function (Asset_Type, Asset_Id, packageId, packageitemId) {

		var deferred = Q.defer();

		var options = {
			url: "https://transporter-node-dev.portqii.com/index.php/package_info/getchildinfo_new?Asset_Id=" + Asset_Id + "&Asset_Type=" + Asset_Type + "&Package_Id=" + packageId + "&Package_Item_Id=" + packageitemId + "",
			method: 'GET',
		};
		//console.log(options.url);
		request(options)
			.then(function (result) {
				//console.log(result);
				console.log(++totalChild +"for "+packageitemId);
				var result = deploy.removeDups(JSON.parse(result));
				deferred.resolve(result);
			});
		return deferred.promise;
	}

	deploy.addOneLevelAssetToPackage = function (packageId, Asset_Id, assetType, assetName) {

		var deferred = Q.defer();

		var options = {
			url: "https://transporter-node-dev.portqii.com/index.php/Deploy/AddOnelevelMissingChildAsset?packageId=" + packageId + "&assetId=" + Asset_Id + "&assetType=" + assetType + "&assetName=" + assetName + "",
			method: 'GET',
		};

		request(options)
			.then(function (result) {
				//console.log(++totalChild);
				//var result = deploy.removeDups(JSON.parse(result));
				deferred.resolve();
			});
		return deferred.promise;
	}



	deploy.removeDups = function (childs) {
		var found = false;
		var newArray = [];
		for (var i = 0; i < childs.length; i++) {
			for (var j = i + 1; j < childs.length; j++) {
				if (i != j) {
					if ((childs[i].Asset_Id == childs[j].Asset_Id) && (childs[i].Asset_Type == childs[j].Asset_Type)) {
						childs.splice(j, 1);
					}
				}
			}
		}
		return childs;
	}

	deploy.makeAssignMicrositeMandatory = function (packageId) {
		var deferred = Q.defer();
		var query = "select * from deployment_package_item where Deployment_Package_Id = " + packageId + " and Asset_Type = 'Landing Page'" + " and verified = 1";
		db.query(query)
			.then(function (result) {
				async.forEach(result, function (item, callback) {
					if (item.JSON_Asset) {


						if (item.JSON_Asset.match('\micrositeId\b') && item.Target_Microsite_JSON) {
							callback();
						} else {
							var message = 'Microsite is not assigned';
							self.updateDeploymentPackage(packageId).then(function (updated) {
								self.updateDeploymentPackageItem(item.Deployment_Package_Item_Id, '', message).then(function (updated) {
									callback();
								}, callback);
							}, callback);
						}
					}
					else {
						callback();
					}
				}, function (err) {
					if (err) {
						deferred.reject(err);
					} else {
						deferred.resolve();
					}
				})
			})
		return deferred.promise;
	}

	deploy.CheckAssetInTarget = function (packageId, assetType, assetName) {
		var deferred = Q.defer();
		var userInfo = {};
		var orgDetails = {};
		var self = this;
		var assetUrl;
		self.getInstanceDetails(packageId, 'target')
			.then(function (details) {
				if (details.length > 0) {
					orgDetails = details[0];
					userInfo.oauth = true;
					userInfo.token = orgDetails.accessToken;
					userInfo.refreshToken = orgDetails.refreshToken;
					self.getAssetEndpoint(assetType)
						.then(function (endpoint) {
							if (endpoint) {
								var str = assetName;
								str = str.replace(/[^A-Za-z0-9]/gi,'*');
								str = '*'+str+'*';
								if (assetType == 'Email' || assetType == 'Form' || assetType == 'Campaign') {
									assetUrl = orgDetails.baseUrl + endpoint[0].endpointUrl + "?search='" + encodeURIComponent(str) + "'&depth=complete";
								}
								else {
									assetUrl = orgDetails.baseUrl + endpoint[0].endpointUrl + "?search='" + encodeURIComponent(str) + "'";
								}

								userInfo.host = assetUrl;
								eloquaCallBasic.get(userInfo)
									.then(function (result) {
										// console.log(assetUrl);
										// console.log(result)
										result = JSON.parse(result);
										var tempElements = result.elements;
										let list = _.filter(tempElements, {name: assetName});
										result.elements = list;
										result.total =  list.length
										if ((result.total) && (result.total > 0)) {
											deferred.resolve(1)
										}
										else {
											deferred.resolve(0);
										}

									});

							}
							else {

							}
						});
				}
			});
		return deferred.promise;
	}

	deploy.updateMissingElements = function (packageId, self) {
		var self = this;
		var deferred = Q.defer();
		var packageItems = [];
		var orgDetails = {};
		var userInfo = {};
		//	var self = this;
		self.getPackageItems(packageId)
			.then(function (items) {
				//console.log(items);
				packageItems = items;
				return self.getInstanceDetails(packageId, 'target');
			})
			.then(function (details) {
				//console.log(details);
				if (details.length > 0) {
					orgDetails = details[0];

					userInfo.oauth = true;
					userInfo.token = orgDetails.accessToken;
					userInfo.refreshToken = orgDetails.refreshToken;

					async.forEachLimit(packageItems, 3, function (item, mainCallback) {
						var assetUrl = "";
						self.getAssetEndpoint(item.Asset_Type)
							.then(function (endpoint) {
								if (endpoint) {
									var str = item.Asset_Name;
									str = str.replace('/[^A-Za-z0-9]/','*');
									if (item.Asset_Type == 'Email' || item.Asset_Type == 'Form' || item.Asset_Type == 'Campaign') {
										assetUrl = orgDetails.baseUrl + endpoint[0].endpointUrl + "?search='" + encodeURIComponent(str) + "'&depth=complete";
									}
									else {
										assetUrl = orgDetails.baseUrl + endpoint[0].endpointUrl + "?search='" + encodeURIComponent(str) + "'";
									}

									userInfo.host = assetUrl;
									//console.log(userInfo.host);
									eloquaCallBasic.get(userInfo)
										.then(function (result) {
											//console.log('result');
											//console.log(result);
											result = JSON.parse(result);
											var tempElements = result.elements;
											let list = _.filter(tempElements, {name: item.Asset_Name});
											result.elements = list;
											result.total =  list.length
											if ((result.total) && (result.total > 0)) {
												if (result.total > 1) {
													if (item.verified == 1) {
														var duplicates = [];
														var duplicate = {};
														var duplicate_E = {};
														var duplicate_F = {};
														if (item.Asset_Type == 'Email') {
															var sourceJson = JSON.parse(item.JSON_Asset);
															var sourceEmailGroupId = sourceJson.emailGroupId;
															var sourceEmailName = sourceJson.name;
															//if(sourceEmailGroupId != undefined){
															self.getEmailGroupNameById(sourceEmailGroupId, packageId, 'source')

																.then(function (sourceEmailGroupName) {
																	var duplicateEmails = [];
																	var duplicateEmail = {};
																	duplicates = [];
																	async.forEach(result.elements, function (item2, callback) {
																		if (item2.type == 'Email') {
																			duplicate_E = {};
																			duplicate_E.Asset_Name = item2.name;
																			duplicate_E.Asset_Id = item2.id;
																			duplicate_E.Asset_Type = item2.type;
																			if (item2.emailGroupId) {
																				self.getEmailGroupNameById(item2.emailGroupId, packageId, 'target')
																					.then(function (targetEmailGroupName) {
																						if ((sourceEmailName == item2.name) && (sourceEmailGroupName == targetEmailGroupName)) {
																							duplicate_E.Status = 'Same';
																						}
																						else {
																							duplicate_E.Status = 'Different';
																						}
																						duplicates.push(duplicate_E);
																						callback();

																					});
																			}
																			else {
																				duplicate_E.Status = 'No EG';
																				duplicates.push(duplicate_E);
																				callback();
																			}
																		}
																		else {
																			callback();
																		}
																	}, function (err) {
																		var status = "";
																		if (item.Target_Asset_Id === "" || item.Target_Asset_Id == 0) {
																			status = 'Duplicate';
																		}
																		self.UpdateDuplicatesArray(duplicates, item.Deployment_Package_Item_Id, status)
																			.then(function (result1) {
																				//console.log(result)
																				if (item.Target_Asset_Id > 0) {
																					self.updateTargetAssetIdDPValidationList(item.Target_Asset_Id, packageId, item.Asset_Type, item.Asset_Id);
																				}
																				else {
																					self.updateTargetAssetIdDPValidationList(result.elements[0].id, packageId, item.Asset_Type, item.Asset_Id);
																				}
																				mainCallback();
																			})
																			.catch(function (err) {
																				//console.log(err);
																			});
																	});
																})
															// }
															// else{
															// 	mainCallback();
															// }

														}
														else if (item.Asset_Type == 'Form') {
															var sourceFormJSON = JSON.parse(item.JSON_Asset);
															var sourceFormName = sourceFormJSON.name;
															var sourceFormHtmlName = sourceFormJSON.htmlName;
															//console.log('result.elements');
															//console.log(result.elements);
															duplicates = [];
															async.forEach(result.elements, function (item3, callback) {
																if (item3.type == 'Form') {
																	duplicate_F = {};
																	duplicate_F.Asset_Name = item3.name;
																	duplicate_F.Asset_Id = item3.id;
																	duplicate_F.Asset_Type = item3.type;
																	if (item3.htmlName) {
																		if ((sourceFormName == item3.name) && (sourceFormHtmlName == item3.htmlName)) {
																			duplicate_F.Status = 'Same';
																		}
																		else {
																			duplicate_F.Status = 'Different'
																		}
																	}
																	else {
																		duplicate_F.Status = 'No htmlName';
																	}
																	duplicates.push(duplicate_F);
																	callback();
																}
																else {
																	callback();
																}

															}, function (err) {
																var status = "";
																if (item.Target_Asset_Id === "" || item.Target_Asset_Id == 0) {
																	status = 'Duplicate';
																}
																self.UpdateDuplicatesArray(duplicates, item.Deployment_Package_Item_Id, status)
																	.then(function (res) {
																		////console.log(result)
																		if (item.Target_Asset_Id > 0) {
																			self.updateTargetAssetIdDPValidationList(item.Target_Asset_Id, packageId, item.Asset_Type, item.Asset_Id);
																		}
																		else {
																			self.updateTargetAssetIdDPValidationList(result.elements[0].id, packageId, item.Asset_Type, item.Asset_Id);
																		}
																		mainCallback();
																	})
																	.catch(function (err) {
																		//console.log(err);
																	});
															});

														}
														else {
															duplicates = [];
															async.forEach(result.elements, function (item, callbackx) {
																duplicate = {};
																duplicate.Asset_Name = item.name;
																duplicate.Asset_Id = item.id;
																duplicates.push(duplicate);
																callbackx();
															}, function (err) {
																var status = "";
																if (item.Target_Asset_Id === "" || item.Target_Asset_Id == 0) {
																	status = 'Duplicate';
																}
																self.UpdateDuplicatesArray(duplicates, item.Deployment_Package_Item_Id, status)
																	.then(function (result1) {
																		if (item.Target_Asset_Id > 0) {
																			self.updateTargetAssetIdDPValidationList(item.Target_Asset_Id, packageId, item.Asset_Type, item.Asset_Id);
																		}
																		else {
																			self.updateTargetAssetIdDPValidationList(result.elements[0].id, packageId, item.Asset_Type, item.Asset_Id);
																		}
																		mainCallback();
																	})
																	.catch(function (err) {
																		//console.log(err);
																	});
															});

														}
														//handle for updating duplicates
													}
													else {
														mainCallback();
													}



												}
												else {
													self.updateMissingTargetFlag(0, item.Deployment_Package_Item_Id)
														.then(function (result) {
															////console.log(result)
															//mainCallback();
														}, function (err) { });
													self.updateTargetAssetIdDPValidationList(result.elements[0].id, packageId, item.Asset_Type, item.Asset_Id);
													mainCallback();
												}
											}
											else {
												self.updateMissingTargetFlag(1, item.Deployment_Package_Item_Id)
													.then(function (result) {

														mainCallback();
													}, function (err) {
														//console.log(err);

													})

											}
										})
										.catch(function (err) {
											console.log(err);
										});

								}
								else {
									//console.log('No Endpoint');
									mainCallback();
								}
							})
							.catch(function (err) {
								//console.log(err);
							});
					}, function (err) {
						//console.log(err);
						self.handleOneLevelChildAssetDuplicates(packageId)
							.then(function (res) {
								deferred.resolve();
							})
					});
				}
			})
			.catch(function (err) {
				//console.log(err);
			});
		return deferred.promise;
	}

	deploy.handleOneLevelChildAssetDuplicates = function (packageId) {
		var self = this;
		var deferred = Q.defer();
		var packageItems = [];
		var orgDetails = {};
		var userInfo = {};
		var duplicates = [];
		var counting = 0;
		self.getVerifiedPackageItems(packageId)
			.then(function (result) {
				packageItems = result;
				return self.getInstanceDetails(packageId, 'target');
			})
			.then(function (details) {
				if (details.length > 0) {
					orgDetails = details[0];

					userInfo.oauth = true;
					userInfo.token = orgDetails.accessToken;
					userInfo.refreshToken = orgDetails.refreshToken;

					async.forEachLimit(packageItems, 3, function (item, mainCallback) {
						self.getDeployPackageValidationList(item.Deployment_Package_Item_Id)
							.then(function (dpvlist) {
								async.forEachLimit(dpvlist, 3, function (dpvItem, mainCallback2) {
									var assetUrl = "";
									self.getAssetEndpoint(dpvItem.Asset_Type)
										.then(function (endpoint) {
											if (endpoint) {
													assetUrl = orgDetails.baseUrl + endpoint[0].endpointUrl + "?search='" + encodeURIComponent(dpvItem.Asset_Name) + "'";
												var str = dpvItem.Asset_Name;
												//str = str.replace(/-/g,'_');
												str = str.replace(/[^A-Za-z0-9]/gi,'*');
												str = '*'+str+'*';
												if (dpvItem.Asset_Type == 'Email' || dpvItem.Asset_Type == 'Form' || dpvItem.Asset_Type == 'Campaign') {
													assetUrl = orgDetails.baseUrl + endpoint[0].endpointUrl + "?search='" + encodeURIComponent(str) + "'&depth=complete";
												}
												else {
													assetUrl = orgDetails.baseUrl + endpoint[0].endpointUrl + "?search='" + encodeURIComponent(str) + "'";
												}
												userInfo.host = assetUrl;
												eloquaCallBasic.get(userInfo)
													.then(function (resultDpvl) {
														//console.log('validated till '+ ++counting);
														resultDpvl = JSON.parse(resultDpvl);
														var tempElements = resultDpvl.elements;
														let list = _.filter(tempElements, {name: dpvItem.Asset_Name});
														resultDpvl.elements = list;
														resultDpvl.total =  list.length;
														if ((resultDpvl) && (resultDpvl.total > 1)) {
															self.addOneLevelAssetToPackage(packageId, dpvItem.Asset_Id, dpvItem.Asset_Type, dpvItem.Asset_Name)
																.then(function () {
																	var duplicates_pvl = [];
																	var duplicate_pvl = {};
																	if (dpvItem.Asset_Type == 'Email') {
																		var sourceJson = {};
																		var sourceEmailGroupId = "";
																		var sourceEmailGroupName = "";
																		var sourceEmailName = "";
																		self.getAssetJSONByIdType(packageId, dpvItem.Asset_Type, dpvItem.Asset_Id)
																			.then(function (json_result) {
																				sourceJson = JSON.parse(json_result[0].JSON_Asset);
																				sourceEmailGroupId = sourceJson.emailGroupId;
																				sourceEmailName = sourceJson.name;
																				if (sourceEmailGroupId != undefined) {
																					return self.getEmailGroupNameById(sourceEmailGroupId, packageId, 'source')
																				}
																			})
																			.then(function (groupName) {
																				if (groupName != undefined) {
																					sourceEmailGroupName = groupName;
																					async.forEach(resultDpvl.elements, function (targetItem, callback) {
																						if (targetItem.type == 'Email') {
																							var duplicate_E = {};
																							duplicate_E.Asset_Name = targetItem.name;
																							duplicate_E.Asset_Id = targetItem.id;
																							duplicate_E.Asset_Type = targetItem.type;
																							if (targetItem.emailGroupId) {
																								self.getEmailGroupNameById(targetItem.emailGroupId, packageId, 'target')
																									.then(function (targetEmailGroupName) {
																										if ((sourceEmailName == targetItem.name) &&
																											(sourceEmailGroupName == targetEmailGroupName)) {
																											duplicate_E.Status = 'Same';
																										}
																										else {
																											duplicate_E.Status = 'Different'
																										}
																										duplicates.push(duplicate_E);
																										callback();
																									});

																							}
																							else {
																								duplicate_E.Status = 'No EG';
																								duplicates.push(duplicate_E);
																								callback();
																							}
																						}
																						else {
																							callback();
																						}
																					}, function (err) {
																						self.updateDuplicatesOfOneLevelChild(duplicates, item, dpvItem)
																							.then(function (res) {

																								self.updateTargetAssetIdDPValidationList(resultDpvl.elements[0].id, packageId, dpvItem.Asset_Type, dpvItem.Asset_Id);

																								mainCallback2();
																							})

																					});

																				}
																				else {
																					mainCallback2();
																				}
																			})

																	}
																	else if (dpvItem.Asset_Type == 'Form') {
																		async.forEach(resultDpvl.elements, function (targetItem, callback) {
																			var sourceFormJSON = {};
																			var sourceFormName = "";
																			var sourceFormHtmlName = "";
																			self.getAssetJSONByIdType(packageId, dpvItem.Asset_Type, dpvItem.Asset_Id)
																				.then(function (json_result) {
																					sourceFormJSON = JSON.parse(json_result[0].JSON_Asset);
																					sourceFormName = sourceFormJSON.name;
																					sourceFormHtmlName = sourceFormJSON.htmlName;
																					if (targetItem.type == 'Form') {
																						var duplicate_F = {};
																						duplicate_F.Asset_Name = targetItem.name;
																						duplicate_F.Asset_Type = targetItem.type;
																						duplicate_F.Asset_Id = targetItem.id;
																						if (targetItem.htmlName) {
																							if ((sourceFormName == targetItem.name) &&
																								(sourceFormHtmlName == targetItem.htmlName)) {
																								duplicate_F = 'Same';
																							}
																							else {
																								duplicate_F.Status = 'Different'
																							}
																							duplicates.push(duplicate_F);
																							callback();

																						}
																						else {
																							duplicate_F.Status = 'No htmlName';
																							duplicates.push(duplicate_F);
																							callback();
																						}
																					}
																					else {
																						callback();
																					}

																				})
																		}, function (err) {
																			self.updateDuplicatesOfOneLevelChild(duplicates, item, dpvItem)
																				.then(function (res) {
																					self.updateTargetAssetIdDPValidationList(resultDpvl.elements[0].id, packageId, dpvItem.Asset_Type, dpvItem.Asset_Id);
																					mainCallback2();
																				})

																		});
																	}
																	else {
																		duplicates = [];
																		async.forEach(resultDpvl.elements, function (targetItem, callbackx) {
																			var duplicate = {};
																			duplicate.Asset_Type = targetItem.type;
																			duplicate.Asset_Id = targetItem.id;
																			duplicate.Asset_Name = targetItem.name;
																			duplicates.push(duplicate);
																			callbackx();
																		}, function (err) {
																			self.updateDuplicatesOfOneLevelChild(duplicates, item, dpvItem)
																				.then(function (res) {
																					self.updateTargetAssetIdDPValidationList(resultDpvl.elements[0].id, packageId, dpvItem.Asset_Type, dpvItem.Asset_Id);
																					mainCallback2();
																				})
																		});

																	}
																});

														}
														else {
															if (resultDpvl.total == 1) {
																self.updateTargetAssetIdDPValidationList(resultDpvl.elements[0].id, packageId, dpvItem.Asset_Type, dpvItem.Asset_Id);
															}
															mainCallback2();
														}
													})
													.catch(function (err) {
														console.log(err);
														mainCallback2();
													});
											}
											else {
												//console.log('No endpoint');
												mainCallback2();
											}

										});

								}, function (err) {
									mainCallback();
								});

							});

					}, function (err) {
						self.updateVerifiedforMissingElements(packageId)
							.then(function (res) {
								// self.updateMissingAssetJsonAndStatus(packageId)
								// 	.then(function (res2) {
								deferred.resolve();
								// });
							})
					});
				}


			});
		return deferred.promise;
	}

	deploy.updateMissingAssetJsonAndStatus = function (packageId) {
		var self = this;
		var deferred = Q.defer();
		var userInfo = {};
		deploy.getPackageItems(packageId)
			.then(function (result) {
				packageItems = result;
				return deploy.getInstanceDetails(packageId, "source")
			})
			.then(function (details) {
				orgDetails = details[0];
				userInfo.oauth = true;
				userInfo.token = orgDetails.accessToken;
				userInfo.refreshToken = orgDetails.refreshToken;
				async.forEachLimit(packageItems, 3, function (item, mainCallback) {
					var assetUrl = "";
					self.getAssetEndpointSingle(item.Asset_Type)
						.then(function (endpoint) {
							if (endpoint) {
								if (item.Asset_Type == 'Email' || item.Asset_Type == 'Form' || item.Asset_Type == 'Campaign') {
									assetUrl = orgDetails.baseUrl + endpoint[0].endpointUrl + "/" + item.Asset_Id + "?depth=complete";
								}
								else {
									assetUrl = orgDetails.baseUrl + endpoint[0].endpointUrl + "/" + item.Asset_Id + "";
								}

								userInfo.host = assetUrl;
								//console.log(userInfo.host);
								eloquaCallBasic.get(userInfo)
									.then(function (result) {
										self.updateAssetJsonAndStatus(item.Deployment_Package_Item_Id, result)
											.then(function () {
												mainCallback();
											});
									});
							}
						})
				}, function (err) {
					deferred.resolve();
				});
			})

		return deferred.promise;
	}

	deploy.updateVerifiedforMissingElements = function (packageId) {
		var deferred = Q.defer();
		var self = this;
		var packageItems = [];
		var orgDetail = {};
		deploy.getPackageItems(packageId)
			.then(function (result) {
				packageItems = result;
				return deploy.getInstanceDetails(packageId, "source")
			})
			.then(function (details) {
				orgDetail = details[0];
				packageItems.forEach(function (item, index) {
					self.updateVerifiedFlag(orgDetail.auto_include_missing_assets, item.Deployment_Package_Item_Id)
						.then(function (res) {
							deferred.resolve();
						});
				});
			})

		return deferred.promise;
	}
	deploy.getEmailGroupNameById = function (groupId, packageId, destination) {
		var deferred = Q.defer();
		var self = this;
		if (groupId == undefined) {
			deferred.resolve();
		}
		else {
			self.getInstanceDetails(packageId, destination)
				.then(function (instanceDetails) {
					self.getAssetEndpointSingle('Email Group')
						.then(function (endpoint) {
							var userInfo = {};
							userInfo.oauth = true;
							userInfo.token = instanceDetails[0].accessToken;
							userInfo.refreshToken = instanceDetails[0].refreshToken;
							userInfo.host = instanceDetails[0].baseUrl + endpoint[0].endpointUrl + '/' + groupId;
							eloquaCallBasic.get(userInfo)
								.then(function (result) {
									result = JSON.parse(result);
									if ((result)) {
										deferred.resolve(result.name);
									}
									else {
										deferred.resolve('');
									}
								})
								.catch(function (err) {
									console.log(err);
								});

						});
				});
		}


		return deferred.promise;
	}

	deploy.updateUnsupportedAssets = function (packageId, self) {
		
		var deferred = Q.defer();
		var query = "select JSON_Asset,Deployment_Package_Id,Deployment_Package_Item_Id,Asset_Type,Asset_Name,Asset_Id from deployment_package_item where Deployment_Package_Id = " + packageId + " and Verified = 1";
		db.query(query)
			.then(function (result) {
				if (result.length > 0) {
					async.forEachLimit(result, 1, function (item, callback) {
						if (item.JSON_Asset) {
							//console.log(item.Asset_Name);
							item.JSON_Asset = JSON.parse(item.JSON_Asset);
							query = "select * from rsys_asset_type WHERE Asset_Type_Name='" + item.Asset_Type + "'";
							db.query(query)
								.then(function (atresult) {
									if (atresult.length > 0) {
										if (atresult[0].Unsupported_JSON_Path && atresult[0].Unsupported_JSON_Path.length) {
											var Unsupported_JSON_Paths = atresult[0].Unsupported_JSON_Path.split(',');
											if (Unsupported_JSON_Paths.length > 0) {
												async.forEachLimit(Unsupported_JSON_Paths,1, function (path, subcallback) {
													var replacekey = path.substring(1, path.indexOf('.') + 1)
													replacekey = replacekey.trim();
													var replacekeys = path.split('|');
													//var jsonPath = '$..'+replaceKeys[0];
													var jsonPath = replacekeys[0];

													var replaceList = jp.query(item.JSON_Asset, jsonPath);
													//console.log(replaceList)
													if (replaceList.length > 0) {
														self.updateDeploymentPackage(packageId)
															.then(function (updated) {
																//console.log(replacekeys[1]);
																self.updateDeploymentPackageItem(item.Deployment_Package_Item_Id, path, replacekeys[1])
																	.then(function (updated) {
																		subcallback();
																	});
															}, subcallback);
													} else {
														subcallback();
													}
												}, function (err) {
													if (err) {
														callback(err);
													} else {
														callback();
													}
												})
											} else {
												callback();
											}
										} else {
											callback();
										}
									} else {
										callback();
									}
								}, callback);
						}
						else {
							callback();
						}
					}, function (err) {
						if (err) {
							//console.log(err);
							deferred.reject(err);
						} else {
							deferred.resolve();
						}
					})
				} else {
					deferred.resolve();
				}
			}, function (err) {
				deferred.reject(err);
			});
		return deferred.promise;
	}


	deploy.checkDuplicatehtmlName = function (packageId, self) {
		var deferred = Q.defer();
		var query = "select JSON_Asset,Deployment_Package_Id,Deployment_Package_Item_Id,Asset_Type,Asset_Name,Asset_Id from deployment_package_item where Deployment_Package_Id = " + packageId + " and Asset_Type = 'Form' and Verified = 1";
		db.query(query)
			.then(function (result) {
				if (result.length > 0) {
					self.getPackage(packageId)
						.then(function (Package_details) {
							if (Package_details[0].Target_Site_Name) {
								self.getInstance(Package_details[0].Target_Site_Name)
									.then(function (instance_details) {
										async.forEach(result, function (item, callback) {
											if (item.JSON_Asset) {


												item.JSON_Asset = JSON.parse(item.JSON_Asset);
												self.getDuplicateFormHTMLName(instance_details, item.JSON_Asset.htmlName, item.Asset_Name, packageId).then(function (response) {
													if (response >= 1) {
														self.updateDeploymentPackageItem(item.Deployment_Package_Item_Id, '', 'htmlname already exists in the target').then(function (updated) {
															self.updateDeploymentPackage(packageId).then(function (updated) {
																callback();
															}, callback);
														}, callback)
													} else {
														callback();
													}
												}, callback);
											}
											else {
												callback();
											}
										}, function (err) {
											if (err) {
												deferred.reject(err);
											} else {
												deferred.resolve();
											}
										})
									}, function (err) {
										deferred.reject(err);
									});
							} else {
								deferred.reject(new Error("package not found"));
							}
						}, function (err) {
							deferred.reject(err);
						});
				} else {
					deferred.resolve();
				}
			}, function (err) {
				deferred.reject(err);
			});
		return deferred.promise;
	}

	deploy.getDuplicateFormHTMLName = function (instance_details, htmlName, assetName, packageId) {
		var deferred = Q.defer();
		var self = this;
		var token = instance_details[0].Token;
		var Base_Url = instance_details[0].Base_Url;
		var targetOrg = instance_details[0];
		var userInfo = {};
		self.getInstanceDetails(packageId, 'target')
			.then(function (details) {

				userInfo.oauth = true;
				userInfo.token = details[0].accessToken;
				userInfo.refreshToken = details[0].refreshToken;

				var query = "select * from rsys_asset_endpoint rae WHERE rae.Asset_Type = 'Form' and rae.Endpoint_Type = 'Read List'";
				db.query(query)
					.then(function (endpoint) {
						var url = Base_Url + endpoint[0].Endpoint_URL + '?depth=complete&search="' + htmlName + '"';
						userInfo.host = url;
						eloquaCallBasic.get(userInfo)
							.then(function (result) {
								result = JSON.parse(result);
								if (result.elements && result.elements[0]) {
									if (result.elements[0].name == assetName) {
										deferred.resolve(0)
									} else {
										deferred.resolve(result.total);
									}
								} else {
									deferred.resolve(0);
								}
							})
							.catch(function (err) {
								console.log(err);
							});
					}, function (err) {
						deferred.reject(err);
					});
			});
		return deferred.promise;
	}

	deploy.EmailWithDifferentEG = function (packageId, self) {
		//	var self = this;
		var deferred = Q.defer();
		var query = "select JSON_Asset,Deployment_Package_Id,Deployment_Package_Item_Id,Asset_Type,Asset_Name,Asset_Id from deployment_package_item where Deployment_Package_Id = " + packageId + " and Asset_Type = 'Email' and Verified = 1";
		db.query(query)
			.then(function (result) {
				if (result.length > 0) {
					var emailResults = [];
					async.forEachOf(result, function (item, index, callback) {
						if (item.JSON_Asset) {
							item.JSON_Asset = JSON.parse(item.JSON_Asset);
							//emailResults[index] = {};
							if (item.JSON_Asset && item.JSON_Asset.emailGroupId != undefined) {
								self.getEmailGroupNameById(item.JSON_Asset.emailGroupId, packageId, 'source')
									.then(function (emailgroupname) {
										emailResults[index] = {};
										emailResults[index].EmailName = item.JSON_Asset.name;
										emailResults[index].EmailGroupName = emailgroupname;
										emailResults[index].Deployment_Package_Item_Id = item.Deployment_Package_Item_Id;
										callback();
									}, callback)
							} else {
								callback();
							}
						}
						else {
							callback();
						}
					}, function (err) {
						if (err) {
							deferred.reject(err);
						} else {
							async.forEach(emailResults, function (emailresult, endcallback) {
								self.getTargetEmailByName(emailresult.EmailName, packageId, emailresult.EmailGroupName)
									.then(function (response) {
										if (response >= 1) {
											self.updateDeploymentPackageItem(emailresult.Deployment_Package_Item_Id, '', 'Email with different email group exists in the target').then(function (updated) {
												self.updateDeploymentPackage(packageId).then(function (updated) {
													endcallback();
												}, endcallback)
											}, endcallback)
										}
										else {
											endcallback();
										}
									}, endcallback);
							}, function (err) {
								if (err) {
									deferred.reject(err);
								} else {
									deferred.resolve();
								}
							});
						}
					})
				} else {
					deferred.resolve();
				}
			}, function (err) {
				deferred.reject(err);
			});
		return deferred.promise;
	}

	deploy.getTargetEmailByName = function (emailName, packageId, EmailGroupName) {
		var deferred = Q.defer();
		var self = this;
		var userInfo = {};
		self.getInstanceDetails(packageId, 'target')
			.then(function (details) {

				userInfo.oauth = true;
				userInfo.token = details[0].accessToken;
				userInfo.refreshToken = details[0].refreshToken;

				self.getPackage(packageId)
					.then(function (Package_details) {
						if (Package_details[0].Target_Site_Name) {
							self.getInstance(Package_details[0].Target_Site_Name)
								.then(function (instance_details) {
									var token = instance_details[0].Token;
									var Base_Url = instance_details[0].Base_Url;
									var targetOrg = instance_details[0];
									var query = "select * from rsys_asset_endpoint WHERE Asset_Type = 'Email' and Endpoint_Type = 'Find Asset'";
									db.query(query).then(function (endpoint) {
										var str = emailName;
										str = str.replace(/[^A-Za-z0-9]/gi,'*');
										str = '*'+str+'*';
										var url = Base_Url + endpoint[0].Endpoint_URL + '?search="' + str + '"&depth=complete';
										userInfo.host = url;
										eloquaCallBasic.get(userInfo)
											.then(function (result) {
												//	console.log(userInfo.host);
												result = JSON.parse(result);
												var tempElements = result.elements;
												let list = _.filter(tempElements, {name: emailName});
												result.elements = list;
												result.total =  list.length
												if (result && result.elements.length > 0) {
													async.forEach(result.elements, function (item, callback) {
														if (!item.emailGroupId) {
															callback();
														} else {
															self.getEmailGroupNameById(item.emailGroupId, packageId, 'target').then(function (emailgroupname) {
																item.EmailGroupName = emailgroupname;
																callback();
															}, callback);
														}
													}, function (err) {
														if (err) {
															deferred.resolve(-1)
														} else {
															var index = _.findIndex(result.elements, function (element) {
																return emailName == element.name && EmailGroupName == element.EmailGroupName;
															});
															if (index >= 0) {
																deferred.resolve(-2)
															} else {
																deferred.resolve(1);
															}
														}
													})
												} else {
													deferred.resolve(-1);
												}
											})
											.catch(function (err) {
												console.log(err);
											});
									}, function (err) {
										deferred.reject(err);
									});
								}, function (err) {
									deferred.reject(err);
								})
						} else {
							deferred.reject(new Error("can't find the package"));
						}
					}, function (err) {
						deferred.reject(err);
					})
			})
		return deferred.promise;
	}


	deploy.checkForCampaignInputs = function (packageId, self) {
		//var self = this;
		var deferred = Q.defer();
		var message = "";
		var query = "select JSON_Asset, Deployment_Package_Id,Deployment_Package_Item_Id,Asset_Type,Asset_Name,Asset_Id from deployment_package_item where Deployment_Package_Id = " + packageId + " and Asset_Type = 'Campaign' and Verified = 1";
		db.query(query)
			.then(function (result) {
				if (result.length > 0) {
					async.forEachLimit(result,1, function (item, callback) {
						if (item.JSON_Asset) {


							item.JSON_Asset = JSON.parse(item.JSON_Asset);
							if (item.JSON_Asset && item.JSON_Asset.elements) {
								async.forEachLimit(item.JSON_Asset.elements,1, function (element, subcallback) {
									if (element.type == 'CampaignInput' && element.source) {
										var sourceType = element.source.type;
										var sourceId = element.source.id;
										self.checkForAssetsIncludedInPackage(packageId, sourceType, sourceId)
											.then(function (IncludedInPackageFlag) {
												if (!IncludedInPackageFlag) {
													message = "";
													message = 'Add the missing ' + sourceType + ' ' + element.source.name + ' to the package';
													self.updateDeploymentPackage(packageId)
														.then(function (response) {
															self.updateDeploymentPackageItem(item.Deployment_Package_Item_Id, '', message).then(function (response) {
																subcallback();
															}, subcallback);
														}, subcallback);
												}
												else {
													subcallback();
												}
											}, function (err) {
												subcallback(err);
											})
									}
									else {
										subcallback();
									}
								}, function (err) {
									if (err) {
										callback(err);
									} else {
										callback();
									}
								});
							} else {
								callback();
							}
						}
						else {
							callback();
						}
					}, function (err) {
						if (err) {
							deferred.reject(err);
						} else {
							deferred.resolve();
						}
					})
				} else {
					deferred.resolve();
				}
			}, function (err) {
				deferred.reject(err);
			});
		return deferred.promise;
	}

	deploy.getTargetAssetJSONBySourceAssetName = function (assetName, assetType, packageId) {
		var self = this;
		var deferred = Q.defer();
		var userInfo = {};
		this.getPackage(packageId).then(function (Package_details) {
			if (Package_details[0].Source_Site_Name) {
				self.getInstanceDetails(packageId, 'target')
					.then(function (details) {
						userInfo.oauth = true;
						userInfo.token = details[0].accessToken;
						userInfo.refreshToken = details[0].refreshToken;
						var query = "select * from rsys_asset_endpoint WHERE Asset_Type = '" + assetType + "' and Endpoint_Type = 'Find Asset'";
						db.query(query).then(function (endpoint) {
							var str = assetName;				
							str = str.replace(/[^A-Za-z0-9]/gi,'*');
							str = '*'+str+'*';
							var url = details[0].baseUrl + endpoint[0].Endpoint_URL + '?search="*' + str + '*"&depth=complete';
							userInfo.host = url;
							eloquaCallBasic.get(userInfo)
								.then(function (result) {
									result = JSON.parse(result);
									var tempElements = result.elements;
									let list = _.filter(tempElements, {name: assetName});
									result.elements = list;
									result.total =  list.length
									if (result && result.elements) {
										if (result.elements[0] && result.elements[0].micrositeId) {
											deferred.resolve("Microsite Found");

										} else {
											deferred.resolve("Microsite Not Found");
										}
									} else {
										deferred.resolve("LP Not Exists");
									}
								})
								.catch(function (err) {
									console.log(err);
								});
						}, function (err) {
							deferred.reject(err);
						});
					}, function (err) {
						deferred.reject(err);
					})
			} else {
				deferred.reject(new Error("can't find the package"));
			}
		}, function (err) {
			deferred.reject(err);
		})
		return deferred.promise;
	}

	deploy.getSourceAssetNameById = function (assetId, assetType, packageId) {
		var self = this;
		var deferred = Q.defer();
		var userInfo = {};
		this.getPackage(packageId)
			.then(function (Package_details) {
				if (Package_details[0].Source_Site_Name) {
					self.getInstanceDetails(packageId, 'source')
						.then(function (details) {
							userInfo.oauth = true;
							userInfo.token = details[0].accessToken;
							userInfo.refreshToken = details[0].refreshToken;
							var query = "select * from rsys_asset_endpoint WHERE Asset_Type = '" + assetType + "' and Endpoint_Type = 'Read Single'";
							db.query(query)
								.then(function (endpoint) {
									var url = details[0].baseUrl + endpoint[0].Endpoint_URL + '/' + assetId;
									userInfo.host = url;
									eloquaCallBasic.get(userInfo)
										.then(function (result) {
											result = JSON.parse(result);
											if (result && result.name) {
												deferred.resolve(result.name);
											} else {
												deferred.reject(new Error("can't find source asset"));
											}
										})
										.catch(function (err) {
											console.log(err);
										});
								}, function (err) {
									deferred.reject(err);
								});
						}, function (err) {
							deferred.reject(err);
						})
				} else {
					deferred.reject(new Error("can't find the package"));
				}
			}, function (err) {
				deferred.reject(err);
			})
		return deferred.promise;
	}

	deploy.checkForLandingPageWithoutMicrositeForChildOfHyperlink = function (packageId, self) {
		//var self = this;
		var deferred = Q.defer();
		var query = "select JSON_Asset,Deployment_Package_Id,Deployment_Package_Item_Id,Asset_Type,Asset_Name,Asset_Id from deployment_package_item where Deployment_Package_Id = " + packageId + " and Asset_Type = 'Hyperlink' and verified = 1";
		db.query(query)
			.then(function (result) {
				if (result.length > 0) {
					async.forEach(result, function (item, callback) {
						if (item.JSON_Asset) {
							item.JSON_Asset = JSON.parse(item.JSON_Asset);
							if (item.JSON_Asset && item.JSON_Asset.referencedEntityId) {
								self.getSourceAssetNameById(item.JSON_Asset.referencedEntityId, 'Landing Page', packageId).then(function (sourceLPName) {
									self.getTargetAssetJSONBySourceAssetName(sourceLPName, 'Landing Page', packageId).then(function (targetMicrositeFoundFlag) {
										if (targetMicrositeFoundFlag == 'Microsite Not Found') {
											self.checkForAssetsIncludedInPackage(packageId, 'Landing Page', item.JSON_Asset.referencedEntityId).then(function (IncludedInPackageFlag) {
												if (!IncludedInPackageFlag) {
													var assetNameArr = item.JSON_Asset.name.split('(');
													var message = 'Add the missing Landing Page ' + assetNameArr[0] + ' to the package';
													self.updateDeploymentPackage(packageId).then(function (response) {
														self.updateDeploymentPackageItem(item.Deployment_Package_Item_Id, '', message)
															.then(function (response) {
																callback();
															}, callback);
													}, callback);
												}
												else {
													callback();
												}
											}, callback);
										}
										else {
											callback();
										}
									}, callback);

								}, callback);
							}
							else {
								callback();
							}
						}
						else {
							callback();
						}
					}, function (err) {
						if (err) {
							deferred.reject(err);
						} else {
							deferred.resolve();
						}
					})
				} else {
					deferred.resolve();
				}
			}, function (err) {
				deferred.reject(err);
			});
		return deferred.promise;
	}

	try {
		deploy.handleStage = function (packageId, queueId) {
			var deferred = Q.defer();
			var self = this;
			var packageItems;
			function generateStage(packageId, parent, maincallback) {
				if (generatestagecount[parent.Deployment_Package_Item_Id] >= 0) {
					generatestagecount[parent.Deployment_Package_Item_Id]++;
				} else {
					generatestagecount[parent.Deployment_Package_Item_Id] = 0;
				}
				//	console.log(JSON.stringify(generatestagecount));
				self.getChildren(parent.Deployment_Package_Item_Id).then(function (children) {

					if (children.length > 0) {
						async.forEach(children, function (child, callback) {
							self.getPackageItem(packageId, child.Asset_Type, child.Asset_Id).then(function (result) {
								if (result && result[0]) {
									var childE = result[0];
									try {
										var index = _.indexOf(packageItems, function (item) {
											return item.Deployment_Package_Item_Id == childE.Deployment_Package_Item_Id;
										});
										if (index >= 0) {
											packageItems.splice(index, 1);
										}
									} catch (e) {
										console.log(e);
									} finally {

									}
									//console.log(stages[childE.Deployment_Package_Item_Id])
									if (!stages[childE.Deployment_Package_Item_Id]) {
										childE.Deploy_Stage = parent.Deploy_Stage + 1;
										stages[childE.Deployment_Package_Item_Id] = {
											order: childE.Deploy_Ordinal,
											stage: childE.Deploy_Stage
										};
										self.updateStage(childE.Deployment_Package_Item_Id, parent.Deploy_Stage + 1, childE.Deploy_Ordinal);
										generateStage(packageId, childE, callback);
									}
									else if (stages[childE.Deployment_Package_Item_Id] && stages[childE.Deployment_Package_Item_Id].stage <= (parent.Deploy_Stage)) {
										childE.Deploy_Stage = parent.Deploy_Stage + 1;
										stages[childE.Deployment_Package_Item_Id] = {
											order: childE.Deploy_Ordinal,
											stage: childE.Deploy_Stage
										};
										self.updateStage(childE.Deployment_Package_Item_Id, parent.Deploy_Stage + 1, childE.Deploy_Ordinal);
										self.updateChildElementStages(childE.Deployment_Package_Item_Id, parent.Deploy_Stage + 1);
										callback();
									}

									else {
										callback();
									}
								} else {
									callback();
								}
							}, function (err) {
								callback();
							});
						}, function (err) {
							maincallback();
						})
					} else {
						maincallback();
					}
				}, function (err) {
					maincallback();
				});
			}

			var stages = {};
			var generatestagecount = {};

			self.getVerifiedPackageItems(packageId).then(function (packageItems) {
				//	console.log(packageItems.length)
				async.forEachLimit(packageItems, 1, function (packageItem, cb) {
					if (!stages[packageItem.Deployment_Package_Item_Id]) {
						self.getChildren(packageItem.Deployment_Package_Item_Id).then(function (children) {
							stages[packageItem.Deployment_Package_Item_Id] = {
								order: packageItem.Deploy_Ordinal,
								stage: 0
							};
							self.updateStage(packageItem.Deployment_Package_Item_Id, 0, packageItem.Deploy_Ordinal);
							if (children.length > 0) {
								async.forEach(children, function (child, callback) {
									self.getPackageItem(packageId, child.Asset_Type, child.Asset_Id).then(function (result) {
										if (result && result[0]) {
											child = result[0];
											try {
												var index = _.indexOf(packageItems, function (item) {
													return item.Deployment_Package_Item_Id == child.Deployment_Package_Item_Id;
												});
												if (index >= 0) {
													packageItems.splice(index, 1);
												}
											} catch (e) {
												console.log(e);
											} finally {

											}
											if (!stages[child.Deployment_Package_Item_Id]) {
												child.Deploy_Stage = 1;
												stages[child.Deployment_Package_Item_Id] = {
													order: child.Deploy_Ordinal,
													stage: 1
												};
												self.updateStage(child.Deployment_Package_Item_Id, 1, child.Deploy_Ordinal);
												generateStage(packageId, child, callback);
											} else if (stages[child.Deployment_Package_Item_Id] && stages[child.Deployment_Package_Item_Id].stage < 1) {
												child.Deploy_Stage = 1;
												stages[child.Deployment_Package_Item_Id] = {
													order: child.Deploy_Ordinal,
													stage: 1
												};
												self.updateStage(child.Deployment_Package_Item_Id, 1, child.Deploy_Ordinal);
												self.updateChildElementStages(child.Deployment_Package_Item_Id, 1);
												callback();
											}

											else {
												callback();
											}
										} else {
											console.log("package not found " + child.Asset_Id);
											callback();
										}
									}, function (err) {
										console.log(err, "package not found " + child.Asset_Id);
										callback();
									});
								}, function (err) {
									cb();
								})
							}
							else {
								cb();
							}
						}, function (err) {
							console.log(err);
						});
					} else {
						cb();
					}
				}, function () {
					console.log("completed");
					var max = _.max(stages, function (o) { return o.stage; });
					var maxStage = max.stage;
					function deployStage(stage) {
						self.getPackageItemsByStage(packageId, stage).then(function (packageitems) {
							async.forEachLimit(packageitems, 2, function (packageItem, callback) {
								// var test = {};
								// test.text = 'hi';
								// var options = {
								// 	url: 'https://transporter-node-dev.portqii.com/index.php/deploy/deploy_parallel?package_item_id=' + packageItem.Deployment_Package_Item_Id + '',
								// 	method: 'GET',
								// }


								// //console.log(packageItem.Deployment_Package_Item_Id, packageItem.Deploy_Stage);

								callback();
								// request(options)
								// 	.then(function (result) {
								// 		//	console.log(result);
								// 		callback();
								// 	})
								// 	.catch(function (error) {
								// 		console.log(error);
								// 	});
							}, function (err) {
								if (stage == 0) {
									console.log("deploy completed");
									deploy.decideStatus(packageId)
										.then(function (resultant) {
											var endDate1 = new Date();
											var getHours = endDate1.getUTCHours();
											var getMinutes = endDate1.getUTCMinutes();
											endDate1.setHours(getHours + 5);
											endDate1.setMinutes(getMinutes + 30);
											var endDate = dateFormat(endDate1, "yyyy-mm-dd hh:MM:ss");
											var queryQueue = "update deployment_queue SET Status = 'Processed',End_=" + endDate + " where Deployment_Queue_Id=" + queueId;
											db.query(queryQueue);

											var query = "update deployment_package SET Status = '" + resultant + "' WHERE Deployment_Package_Id = " + packageId;
											db.query(query).then(function (response) {
												deferred.resolve();
											}, function (err) {
												deferred.reject(err);
											});



										})
										.catch(function (err) {
											deferred.resolve();
										});


								} else {
									deployStage(stage - 1);
								}
							})
						});

					}

					setTimeout(deployStage(maxStage), 5000);
					//deployStage(maxStage);
				})
			}, function (err) {
				console.log(err);
			});


			return deferred.promise;

		}
	} catch (error) {
		console.log(error)
	}

	deploy.getChildren = function (id) {
		var deferred = Q.defer();
		var query = "select * from deployment_package_validation_list where Deployment_Package_Item_Id = " + id;
		db.query(query)
			.then(function (result) {
				deferred.resolve(result);
			});
		return deferred.promise;
	}


	deploy.getTopPackageItem = function (id) {
		var deferred = Q.defer();
		var query = "select * from deployment_package_item where Deploy_Ordinal = 0 and Deployment_Package_Id = " + id;
		db.query(query)
			.then(function (result) {
				deferred.resolve(result[0]);
			});
		return deferred.promise;
	}

	deploy.getPackageItemsByStage = function (packageId, stage) {
		var deferred = Q.defer();
		var query = "select * from deployment_package_item where Deployment_Package_Id = " + packageId + " and Deploy_Stage=" + stage + " and verified = 1";
		db.query(query)
			.then(function (result) {
				deferred.resolve(result);
			});
		return deferred.promise;
	}

	deploy.getPackageItemsByStageOrder = function (packageId) {
		var deferred = Q.defer();
		var selectQuery = "select Deployment_Package_Item_Id, Deploy_Stage from deployment_package_item where Deployment_Package_Id=" + packageId + " order by Deploy_Stage Desc";
		db.query(selectQuery)
			.then(function (result) {
				deferred.resolve(result);
			});
		return deferred.promise;
	}


	deploy.updateStage = function (Deployment_Package_Item_Id, stage, order) {
		var deferred = Q.defer();
		var updateQuery = "UPDATE deployment_package_item SET Deploy_Stage = " + stage + " WHERE Deployment_Package_Item_Id = " + Deployment_Package_Item_Id;
		//	console.log(updateQuery + " ------ " + order);
		db.query(updateQuery)
			.then(function (result) {
				deferred.resolve(true);
			}, function (err) {
				console.log(err);
			});

		return deferred.promise;
	}

	deploy.updateStageNew = function (Deployment_Package_Item_Id, stage) {
		var deferred = Q.defer();
		var updateQuery = "UPDATE deployment_package_item SET Deploy_Stage = " + stage + " WHERE Deployment_Package_Item_Id = " + Deployment_Package_Item_Id;
		//	console.log(updateQuery + " ------ " + order);
		db.query(updateQuery)
			.then(function (result) {
				deferred.resolve(true);
			}, function (err) {
				console.log(err);
			});

		return deferred.promise;
	}

	deploy.updateAssetStage = function (packageid, assetType, assetId, stage) {
		var deferred = Q.defer();
		var selectQuery = "Select Deploy_Stage from deployment_package_item where Deployment_Package_Id =" + packageid + " and Asset_Type='" + assetType + "' and Asset_Id=" + assetId;
		db.query(selectQuery)
			.then(function (result) {
				if (result && result.length > 0) {
					if (result[0].Deploy_Stage < stage) {
						var updateQuery = "Update deployment_package_item SET Deploy_Stage=" + stage + " where Deployment_Package_Id =" + packageid + " and Asset_Type='" + assetType + "' and Asset_Id=" + assetId;
						db.query(updateQuery)
							.then(function (result) {
								deferred.resolve();
							})
					}
					else {
						deferred.resolve();
					}
				}
				else {
					deferred.resolve();
				}
			})
			.catch(function (err) {

			});
		return deferred.promise;
	}


	deploy.checkStatus = function (packageId) {
		deploy.decideStatusValidate(packageId)
			.then(function (result) {
				//	console(result);
			})
	}

	deploy.assignStageRecurse = function (packageItem, mainCallback) {
		if (packageItem) {
			var currentStage = packageItem.Deploy_Stage;
			var self = this;
			self.getDeployPackageValidationList(packageItem.Deployment_Package_Item_Id)
				.then(function (deploymentPackageValidationList) {
					async.forEachLimit(deploymentPackageValidationList, 1, function (item, callback2) {
						self.updateAssetStage(packageItem.Deployment_Package_Id, item.Asset_Type, item.Asset_Id, currentStage + 1)
						.then(function(result2){
							self.getPackageItem(packageItem.Deployment_Package_Id, item.Asset_Type, item.Asset_Id)
							.then(function (childPackageItem) {
								if (childPackageItem.length > 0) {
									self.assignStageRecurse(childPackageItem[0], function () {
										callback2();
									})
								}
								else {
									callback2();
								}

							})
						});
						
					}, function (err) {
						mainCallback();
					})
				})
		}
		else {
			mainCallback();
		}

	}

	deploy.assignStages = function (packageId, self) {
		var deferred = Q.defer();
		//var self = this;
		self.getVerifiedPackageItems(packageId)
			.then(function (packageItems) {
				async.forEachLimit(packageItems, 1, function (item, callback) {
					self.assignStageRecurse(item, function () {
						callback();
					})
				}, function (err) {
					deferred.resolve();
				})
			})

		return deferred.promise;
	}

	deploy.preDeployPackageItems = function (packageId, queueId) {
		var deferred = Q.defer();
		var self = this;
		var maxStage = 0;
		self.getPackageItemsByStageOrder(packageId)
			.then(function (packageItems) {
				if (packageItems.length > 0) {
					maxStage = packageItems[0].Deploy_Stage;
					var result = _.chain(packageItems)
						.groupBy("Deploy_Stage")
						.pairs()
						.map(function (currentItem) {
							return _.object(_.zip(["deployStage", "stageItems"], currentItem));
						})
						.value();
					result = result.reverse();
					async.forEachLimit(result, 1, function (item, callback) {
						var asyncLimit = 3;
						var cfItems = _.filter(item.stageItems,{Asset_Type:"Contact Field"});
						if(cfItems.length>1)
						{
							asyncLimit = 1;
						}
						async.forEachLimit(item.stageItems, 3, function (item2, callback2) {
							self.deployPackageItems(item2.Deployment_Package_Item_Id)
								.then(function (res) {

									callback2();
								})
								.catch(function(err){
									callback2(err);
								})

						}, function (err) {
							if(err){
								callback(err);
							}
							else{
								callback();
							}
						});
					}, function (err) {

						deploy.decideStatus(packageId)
							.then(function (resultant) {
								var endDate1 = new Date();
								var getHours = endDate1.getUTCHours();
								var getMinutes = endDate1.getUTCMinutes();
								endDate1.setHours(getHours + 5);
								endDate1.setMinutes(getMinutes + 30);
								var endDate = dateFormat(endDate1, "yyyy-mm-dd hh:MM:ss");
								var queryQueue = "update deployment_queue SET Status = 'Processed',End_='" + endDate + "' where Deployment_Queue_Id=" + queueId;
								db.query(queryQueue);
								var query = "update deployment_package SET Status = '" + resultant + "' WHERE Deployment_Package_Id = " + packageId;
								db.query(query).then(function (response) {
									deferred.resolve();
								}, function (err) {
									deferred.reject(err);
								});
							})
							.catch(function (err) {
								deferred.resolve();
							});
					});
				}
				else {
					deferred.resolve();
				}
			});
		return deferred.promise;
	}

	deploy.deployPackageItems = function (Deployment_Package_Item_Id) {
		var deferred = Q.defer();
		var options = {
			url: 'https://transporter-node-dev.portqii.com/index.php/deploy/deploy_parallel?package_item_id=' + Deployment_Package_Item_Id + '',
			method: 'GET',
		}
		request(options)
			.then(function (result) {
				if(String(result).includes('package item errored'))
				{
					console.log(result);
					deferred.reject(result);
				}
				else
				{
					//console.log(result);
					deferred.resolve();
				}
				
			})
			.catch(function(err){
				console.log('error at deployPackageItems',err);
				deferred.reject(err);
			})
		return deferred.promise;
	}

})(module.exports);
