

(function(db) {
	var mysql = require('mysql');
	var Q = require('q');

	var con,config,pool;

	// console.log("config ", config);// undefined
	// var pool  = mysql.createPool({
	// 	"host":			"ec2-54-202-113-3.us-west-2.compute.amazonaws.com",
	// 	"user":			"root",
	// 	"password":		"p0rtq1!@Aug",
	// 	"database":		"segmentation"
	// });
	db.init = function(configParam) {
		config = configParam;
		pool = mysql.createPool({
			"host": "transporterdevbackup.cltztkbthuok.us-west-2.rds.amazonaws.com",
			"user": "transporterDB",
			"password": "transporterDevBackup",
			"database": "transporter_QA_Node",
			connectionLimit : 1000,
			connectTimeout  : 60 * 60 * 1000,
			aquireTimeout   : 60 * 60 * 1000,
			timeout         : 60 * 60 * 1000,
		});
		// console.log("pool ", pool);
	}

	db.init();

	db.query = function(queryStatement,params,resultantFunction,largeConnectionStatus) {
		var deferred = Q.defer();
		var parameters = [];
		pool.getConnection(function(err, con){

			
			
			parameters = params;
			if(params && params.length > 0){
				if(typeof(queryStatement) == 'string' && typeof(resultantFunction) == 'function'){
					con.query(queryStatement, parameters,function (err, result) {
						if(err){
							console.error(new Date()+': While running the query '+queryStatement+' Error in database',err);
	
							deferred.reject(err);
							return;
						}
						resultantFunction(result,err,con);
						con.release();
					});
				}else if(typeof(queryStatement) == 'string'){
					con.query(queryStatement,parameters, function (err, result1) {
						if(err){
							console.error(new Date()+': Error in database',err);
							deferred.reject(err);
							return;
						}
						deferred.resolve(result1);
						con.release();
	
					});
				}
				
			}
			else{
				if(typeof(queryStatement) == 'string' && typeof(resultantFunction) == 'function'){
					con.query(queryStatement, function (err, result) {
						if(err){
							console.error(new Date()+': While running the query '+queryStatement+' Error in database',err);
	
							deferred.reject(err);
							return;
						}
						resultantFunction(result,err,con);
						con.release();
					});
				}else if(typeof(queryStatement) == 'string'){
					con.query(queryStatement, function (err, result1) {
						if(err){
							console.error(new Date()+': Error in database',err);
							deferred.reject(err);
							return;
						}
						deferred.resolve(result1);
						con.release();
	
					});
				}

			}
			
		});
		
		return deferred.promise;
	}
})(module.exports);
